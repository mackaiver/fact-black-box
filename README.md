fact-black-box
==============

Black box testing for the fact-tools. This will read a folder of xmlfiles and execute the fact-tools.jar and
checks for the correct output and behaviour of the program

For more info about the Fact-Tools see here  [http://sfb876.tu-dortmund.de/FACT/](http://sfb876.tu-dortmund.de/FACT/)

This repo is mirrored on bitbucket and github as described [here](http://deanclatworthy.com/2013/01/how-to-avoid-relying-on-github-mirror-your-repository/)

This is what my gitconf looks like for the whole mirroring stuff
    [remote "github"]
        url = git@github.com:mackaiver/fact-black-box.git
        fetch = +refs/heads/*:refs/remotes/github/*
    [branch "master"]
        remote = github
        merge = refs/heads/master
    [remote "bitbucket"]
        url = git@bitbucket.org:mackaiver/fact-black-box.git
        fetch = +refs/heads/*:refs/remotes/bitbucket/*

    [remote "origin"]
        url = git@github.com:mackaiver/fact-black-box.git
        url = git@bitbucket.org:mackaiver/fact-black-box.git

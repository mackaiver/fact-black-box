#! /usr/bin/python3

import os
import argparse
import xml.etree.ElementTree as ElementTree
#see gist:  https://gist.github.com/mackaiver/9269113
from colorama import init, Fore, Style
#init colorama so it works on windows as well. The autoreset flag keeps me from using RESET on each line I want to color
init(autoreset=True)

import logging
#create a logger for this module
logger = logging.getLogger(__name__)
#use colorama codes to color diferent output levels
logging.addLevelName( logging.WARNING, Style.BRIGHT + Fore.YELLOW + logging.getLevelName(logging.WARNING) + Style.RESET_ALL)
logging.addLevelName( logging.ERROR, Style.BRIGHT + Fore.RED + logging.getLevelName(logging.ERROR) + Style.RESET_ALL)
#the usual formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# create a handler and make it use the formatter
handler = logging.StreamHandler()
handler.setFormatter(formatter)
# now tell the logger to use the handler
logger.addHandler(handler)


class ProcessExecutor:
    _path_to_jar = None

    def __init__(self, path_to_jar):
        self._path_to_jar = path_to_jar

    def _call_fact_tools(self, description):
        print('Pretending to execute stuff:  ' + str(description))

    @property
    def path_to_jar(self):
        return self._path_to_jar


class InvalidStreamXMLError(Exception):

    def __init__(self):
        super().__init__()


class ProcessDescription:
    xml_data = None
    streams = None
    processes = None
    __iotags = None
    
    def __init__(self, xml_path):
        with open(xml_path, 'r', encoding='utf-8') as xmlFile:
            self.xml_data = xmlFile.read()
            self.parse_xml()

    @classmethod
    def with_file(cls, xml_file):
        cls.xml_data = xml_file.read()
        cls.parse_xml()

    def parse_xml(self):
        tree = ElementTree.XML(self.xml_data)
        self.streams = tree.findall('stream')
        self.processes = tree.findall('process')
        self.__iotags = tree.findall(".//*[@url]")

        if not self.sanity_check():
            raise InvalidStreamXMLError

    #perform a basic check to see if this is even a valid streams xml file
    def sanity_check(self):
        if not self.streams:
            return False
        if not self.processes:
            return False
        return True

    def __str__(self):
        return 'Stream tags:  ' + str(len(self.streams)) + \
               ' Process tags: ' + str(len(self.processes)) + \
               ' IO tags: ' + Fore.GREEN + str(len(self.__iotags))


#checks if the string path points to a valid directory. or a file in a valid directory
def is_valid_file_and_directory(parser, path):
    folder, f = os.path.split(path)

    if f and not os.path.isfile(path):
        parser.error('Not a valid file')

    if not folder == '' and not os.path.exists(folder):
        parser.error('The folder %s does not exist!' % folder)
    else:
        return path


def parse_args():
    p = argparse.ArgumentParser(description='Tests for the Fact-Tools. ',
                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    p.add_argument('-v', action='store_true', default='false',
                   help='Show some nice debug output')
    
    #get a list of possible paths. provided by shell globbing
    p.add_argument('xmlpath', type=lambda arg: is_valid_file_and_directory(p, arg), nargs='+',
                   help='path to the xml files you want to have tested')
    #get path to the jar.
    p.add_argument('jarpath', type=lambda arg: is_valid_file_and_directory(p, arg),
                   help='path to the fact-tools jar')
    
    args = p.parse_args()

    #check for debug logging
    if args.v is True:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    return args.jarpath, args.xmlpath, args.v


def main():

    # jar_path is a single path to jar_path, fs contains a list of paths
    (jar_path, fs, debug) = parse_args()
    logger.debug('xmlpath given: ' + str(fs))
    xmlfile_paths = []

    #iterate through every folder and find all .xml files
    for folder in fs:
        xmlfile_paths = [os.path.join(folder, xmlfile) for xmlfile in os.listdir(folder) if xmlfile.endswith('xml')]

    logger.debug('Found ' + str(len(xmlfile_paths)) + ' files: ' + str(xmlfile_paths))

    descriptions_stack = []
    for x in xmlfile_paths:
        descriptions_stack.append(ProcessDescription(x))

    logger.debug('Getting a ProcessExecutor instance')
    executor = ProcessExecutor(jar_path)
    logger.debug('Start working on the description stack')
    while descriptions_stack:
        d = descriptions_stack.pop()
        executor._call_fact_tools(d)

    logger.debug('Finished execution')


if __name__ == '__main__':
    main()
    



